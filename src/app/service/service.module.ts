import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpService } from './http.service';
import { ListService } from './resolve/list/list.service';
import { DetailService } from './resolve/detail/detail.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    HttpService,
    ListService,
    DetailService
  ]
})
export class ServiceModule { }
