export interface Detail {
    id: String,
    title: String,
    body: String,
    userID: String
}
