import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServiceModule } from './service/service.module';
import { RouterModule } from '@angular/router';
import { RouterConfig } from './app.routing';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FormsModule } from '@angular/forms';

// All components
import { AppComponent } from './app.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';

// All services

@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ServiceModule,
    RouterModule.forRoot(RouterConfig),
    FormsModule,
    AutocompleteLibModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
