import { Routes } from '@angular/router';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';

import { ListService } from './service/resolve/list/list.service';
import { DetailService } from './service/resolve/detail/detail.service';

const Router: Routes = [
    {
        path: '', component: UsersListComponent, pathMatch: 'full', resolve: [
            ListService
        ]
    },
    {
        path: ':id/detail', component: UserDetailsComponent, resolve: [
            DetailService
        ]
    }
]

export const RouterConfig = Router