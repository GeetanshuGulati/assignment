import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { List } from '../interface/list';
import { Detail } from '../interface/detail';
import Routes from '../config';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  getUsersList(): Observable<List[]> {
    return this.http.get<List[]>(Routes.list)
  }

  getUserDetail(userId: String): Observable<Detail[]> {
    return this.http.get<Detail[]>(Routes.detail + '?userId=' + userId)
  }
}
