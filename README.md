Run the project
1) npm install
2) run command ng serve

Modify
1 ng new {project_name} // to generate new project
1 ng g c {component_name} // to generate component
1 ng g s {service_name} // to generate service
1 ng g m {module_name} // to generate module
1 ng g i {interface_name} // to generate interface

Deploy
1) ng build --prod --build-optimizer
2_ firebase deploy 