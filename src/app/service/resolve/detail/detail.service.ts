import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from '../../http.service';
import { Observable } from 'rxjs';
import { Detail } from '../../../interface/detail';

@Injectable({
  providedIn: 'root'
})
export class DetailService implements Resolve<Detail[]>{

  constructor(
    private http: HttpService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Detail[]> {
    return (route.params['id'])
      ? this.http.getUserDetail(route.params['id'])
      : Observable.throw('Please provide user id')
  }
}
