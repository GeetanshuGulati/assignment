import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { HttpService } from '../../http.service';
import { Observable } from 'rxjs';
import { List } from '../../../interface/list';

@Injectable({
  providedIn: 'root'
})
export class ListService implements Resolve<List[]>{

  constructor(
    private http: HttpService
  ) { }

  resolve(): Observable<List[]> {
    return this.http.getUsersList();
  }
}
