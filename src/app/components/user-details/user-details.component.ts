import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  private routeSubscription: Subscription;
  userDetail: any;

  constructor(
    private r: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.routeSubscription = this.r.data.subscribe(data => this.userDetail = data[0].shift())
  }

  ngOnDestroy(): void {
    this.routeSubscription && this.routeSubscription.unsubscribe();
  }

}
