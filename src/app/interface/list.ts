export interface List {
    id: String,
    name: String,
    username: String,
    email: String
}
