import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  private routeService: Subscription
  private options;

  users: any = [];

  constructor(
    private r: Router,
    private route: ActivatedRoute
  ) {
    this.options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      useTextFile: false,
      useBom: true,
      headers: ['name', 'username', 'email']
    };

  }

  ngOnInit(): void {
    this.routeService = this.route.data.subscribe(users => {
      this.users = users[0].map(key => {
        return {
          id: key['id'],
          name: key['name'],
          username: key['username'],
          email: key['email']
        }
      })
    });
  }

  selectEvent(event): void {
    this.r.navigate([`${event['id']}/detail`])
  }

  export(): void {
    const csvExporter = new ExportToCsv(this.options);
    csvExporter.generateCsv(this.users);
  }

  ngOnDestroy(): void {
    this.routeService && this.routeService.unsubscribe()
  }

}
